FROM registry.fedoraproject.org/fedora-minimal:latest as build

WORKDIR /go/src/gitlab.com/simonkrenger/base-gin
RUN microdnf install -y golang git
COPY gin.go go.mod go.sum ./
RUN echo "OK" > healthz
# http://blog.wrouesnel.com/articles/Totally%20static%20Go%20builds/
RUN CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' .
RUN ls -l

FROM scratch
LABEL maintainer="Simon Krenger <simon@krenger.ch>"
LABEL description="A minimal webserver based on the Gin Web Framework"
ENV GIN_MODE release

WORKDIR /webroot
USER 1001

COPY --from=build /go/src/gitlab.com/simonkrenger/base-gin/base-gin /gin
COPY --from=build /go/src/gitlab.com/simonkrenger/base-gin/healthz /webroot/healthz

EXPOSE 8080

CMD ["/gin"]
