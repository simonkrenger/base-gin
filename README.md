# Webserver based on Gin

`base-gin` is an ultra-minimalistic base image based on the [Gin Web Framework](https://github.com/gin-gonic/gin) to serve static files.
It does not feature any pretty error handling or even a default document.

## Usage

The image contains a statically linked binary named `/gin` and will serve static files located under `/webroot/` on port 8080.

~~~
docker run -p 8080:8080 -v $(PWD)/webroot:/webroot registry.gitlab.com/simonkrenger/base-gin:latest
~~~

Or using it in a `Dockerfile`:

~~~
FROM registry.gitlab.com/simonkrenger/base-gin:latest

WORKDIR /webroot

ADD index.html .
~~~

### Healthcheck

The file `/webroot/healthz` exists and is used when executing `/gin healthcheck` in the container.
