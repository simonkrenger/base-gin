package main

import (
	"fmt"

	"github.com/gin-gonic/gin"

	"net/http"
	"os"
)

var SERVE_PORT = "8080"

func main() {

	// Healthcheck (when called with '/gin healthcheck')
	if len(os.Args) > 1 && os.Args[1] == "healthcheck" {
		// Binary was started with healthcheck
		resp, err := http.Get("http://localhost:" + SERVE_PORT + "/healthz")
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		defer resp.Body.Close()
		if resp.StatusCode != 200 {
			fmt.Println("ERROR: Status code was " + resp.Status)
			os.Exit(1)
		}
		os.Exit(0)
	}

	// Regular execution
	router := gin.Default()
	router.Static("/", "/webroot") // Servce static files from /webroot
	router.Run(":" + SERVE_PORT)
}
